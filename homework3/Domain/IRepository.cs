﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public interface IRepository
    {
        Task<IEnumerable<User>> GetUsersAsync();
        Task<IEnumerable<Advertisement>> GetAdvertisementsAsync();
        Task<IEnumerable<Category>> GetCategoriesAsync();
        Task AddUserAsync(User user);
        Task AddAdvertisementAsync(Advertisement advertisement);
        Task AddCategoryAsync(Category category);
    }
}

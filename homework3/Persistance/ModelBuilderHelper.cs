﻿using Domain.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistance
{
    public static class ModelBuilderHelper
    {
        static void SetAccessMode<TEntity>(this ModelBuilder modelBuilder, string name)
            where TEntity : class, IEntity
        {
            modelBuilder.Entity<TEntity>().Metadata.FindNavigation(name)
                ?.SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}

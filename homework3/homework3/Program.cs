﻿using Domain;
using Domain.Entities;
using Microsoft.Extensions.DependencyInjection;
using Persistance;
using Program.Common.Contracts;

namespace Program
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                .AddPersistence()
                .AddTransient<IAvitoService, AvitoService>()
                .AddTransient<UserInteraction>()
                .BuildServiceProvider();

            var userInteraction = serviceProvider.GetService<UserInteraction>() ?? throw new NullReferenceException(nameof(UserInteraction));

            await userInteraction.MainFlowAsync();
        }
    }
}
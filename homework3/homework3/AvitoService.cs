﻿using Domain;
using Domain.Entities;
using Program.Common.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program
{
    internal sealed class AvitoService : IAvitoService
    {
        private readonly IRepository _repository;

        public AvitoService(IRepository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public async Task AddUserAsync(string name, string surname, string lastName,
            string phoneNumber, int age)
        {
            var user = new User(name, surname, lastName, phoneNumber, age);
            await _repository.AddUserAsync(user);
        }

        public async Task AddAdvertisementAsync(string title, string description, DateTime creationDate,
            decimal cost, int userId, int categoryId)
        {
            var advertisement = new Advertisement(title, description, creationDate, cost, userId, categoryId);
            await _repository.AddAdvertisementAsync(advertisement);
        }

        public async Task AddCategoryAsync(string name)
        {
            var category = new Category(name);
            await _repository.AddCategoryAsync(category);
        }

        public async Task ShowTables()
        {
            Console.WriteLine("=========Категории=========\n");
            var categories = await _repository.GetCategoriesAsync();
            foreach (var category in categories)
            {
                Console.WriteLine(GetDataInJson(category));
            }

            Console.WriteLine("=========Пользователи=========\n");
            var users = await _repository.GetUsersAsync();
            foreach (var user in users)
            {
                Console.WriteLine(GetDataInJson(user));
            }

            Console.WriteLine("=========Объявления=========\n");
            var advertisements = await _repository.GetAdvertisementsAsync();
            foreach (var advertisement in advertisements)
            {
                Console.WriteLine(GetDataInJson(advertisement));
            }
        }

        private string GetDataInJson(object instance)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(instance);
        }
    }
}

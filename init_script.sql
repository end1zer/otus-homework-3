CREATE SCHEMA avito;

CREATE TABLE avito.users (
	id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    name text,
    surname text,
    lastname text,
    phone_number text,
    age integer,
	CONSTRAINT users_pk PRIMARY KEY (id)
);

CREATE TABLE avito.categories (
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
	name text,
	CONSTRAINT categories_pk PRIMARY KEY (id)
);

CREATE TABLE avito.advertisements (
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
	title text,
    description text,
    creation_date date with time zone,
    cost numeric,
	user_id integer NOT NULL,
	category_id integer NOT NULL,
	CONSTRAINT advertisements_pk PRIMARY KEY (id),
	CONSTRAINT fk_users FOREIGN KEY (user_id)
		REFERENCES avito.users (id) MATCH SIMPLE,
	CONSTRAINT fk_categories FOREIGN KEY (category_id)
		REFERENCES avito.categories (id) MATCH SIMPLE
);

CREATE INDEX fki_fk_users
    ON avito.advertisements USING btree
    (user_id ASC NULLS LAST)
TABLESPACE pg_default;


CREATE INDEX fki_fk_categories
    ON avito.advertisements USING btree
    (category_id ASC NULLS LAST)
TABLESPACE pg_default;
